Clone the project

git clone "URL"

Running Part1:

Junit test has been added on FilePickerApplication.java
1. Run junit test or main method

Running Part2:

1. Navigate to the project directory ( cd vehicleDetails)
2. Run the below maven command to run the test

mvn test

NOTE: If maven is not installed, follow the below link for the guidance
https://maven.apache.org/install.html

