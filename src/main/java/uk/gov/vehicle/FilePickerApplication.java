package uk.gov.vehicle;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import uk.gov.vehicle.exception.FilePickerException;
import uk.gov.vehicle.model.FileMetaData;
import uk.gov.vehicle.service.FilePickerService;
import uk.gov.vehicle.service.FilePickerServiceImpl;

public class FilePickerApplication {

	public static void main(String[] args) throws IOException, FilePickerException {
		List<FileMetaData> fileMetaDataList = new ArrayList<FileMetaData>();
		String[] mimeTypes = {"csv", "xls"};
		FilePickerService filePickerService = new FilePickerServiceImpl();
		fileMetaDataList = filePickerService.getFileByMimeType(mimeTypes, ".\\testfiles");
		System.out.println(" >>>>>>>>>>> <<<<<<<<<<<<< ");
		for (FileMetaData fmd : fileMetaDataList) {
			System.out.println(" file name = " + fmd.getName());
			System.out.println(" file mime type = " + fmd.getMimeType());
			System.out.println(" file extension = " + fmd.getExtention());
			System.out.println(" file size = " + fmd.getSize());
			System.out.println(" >>>>>>>>>>> <<<<<<<<<<<<< ");
		}
	}

	@SuppressWarnings("unused")
	@Test
	public void getVehicleDetailsInfo() throws FilePickerException {
		List<FileMetaData> fileMetaDataList = new ArrayList<FileMetaData>();
		FilePickerService filePickerService = new FilePickerServiceImpl();
		fileMetaDataList = filePickerService.getFileByMimeType(null, ".\\testfiles");
	}
}