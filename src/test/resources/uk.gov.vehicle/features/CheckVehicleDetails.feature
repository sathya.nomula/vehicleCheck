Feature: Check the vehicle details

  @vehicleCheck
  Scenario Outline: Verify the make and color for the given vehicle registration number
    Given I navigated to the vehicle information website "<URL>"
    When I enter the vehicle registration number and click on submit button

    Examples:
      | URL                                                  |
      | https://www.gov.uk/get-vehicle-information-from-dvla |