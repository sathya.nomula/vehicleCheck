package uk.gov.vehicle.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CheckVehiclePage {
    private WebDriver driver;

    By regNoTextFiled = By.id("Vrm");
    By continueBtn = By.cssSelector("button[name='Continue']");

    public CheckVehiclePage(WebDriver driver) {
        this.driver = driver;
    }

    public void enterRegNo(String regNo) {
        driver.findElement(regNoTextFiled).sendKeys(regNo);
    }

    public void clickOnContinue() {
        driver.findElement(continueBtn).click();
    }

}

