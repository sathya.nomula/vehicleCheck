package uk.gov.vehicle.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class VehicleSummaryPage {
    private WebDriver driver;

    private final By make = By.xpath("//ul[contains(@class,'list-summary')]/li[2]/span/strong");
    private final By color = By.xpath("//ul[contains(@class,'list-summary')]/li[3]/span/strong");
    private final By backLink = By.cssSelector(".back a");

    public VehicleSummaryPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getColor() {
        return driver.findElement(color).getText();
    }

    public String getMake() {
        return driver.findElement(make).getText();
    }

    public void clickOnBackLink() {
        driver.findElement(backLink).click();
    }

}
