package uk.gov.vehicle;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;

public class UtilPage {

    public Map<String, String> getVehicleDetails() {
        String csvFile = "src/data/vehicleInfo.csv";
        Map<String, String> vehicleInfo = new LinkedHashMap<>();
        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                String[] regs = line.split(cvsSplitBy);
                vehicleInfo.put(regs[0], regs[1]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return vehicleInfo;
    }

    public String getMake(String details) {
        return Arrays.asList(details.split("-")).get(0);
    }

    public String getColor(String details) {
        return Arrays.asList(details.split("-")).get(1);
    }
}
